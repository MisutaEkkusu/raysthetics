#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#include <stdint.h>

#include "qdbmp/qdbmp.h"

#define MAX_RAY_DIST 10000.0f
#define INTERSECT_THRESHOLD 0.001f


struct ray{
	
	float ox;		//origin point, mostly only useful for reflections
	float oy;
	float oz;
	
	float dx;		//direction
	float dy;
	float dz;
	
	float ix;		//intersection point
	float iy;
	float iz;
	
	float dist; 	//ray distance
	
	
	
};

struct plane{
	
	float ox;		//plane point
	float oy;
	float oz;
	
	float nx;		//plane normal
	float ny;
	float nz;
	
};

struct sphere{
	
	float ox;
	float oy;
	float oz;
	
	float radius;
	
};

struct sun{
	
	float dx;
	float dy;
	float dz;
	
};

//for shadows, find intersection point for camera ray, then send ray towards light, check for intersection

void RayNormalize(struct ray* r){
	
	float mag = sqrt(r->dx*r->dx + r->dy*r->dy + r->dz*r->dz);
	
	if(mag == 0.0f){
		return;
	}
	
	r->dx /= mag;
	r->dy /= mag;
	r->dz /= mag;
	
}

float dot(float x, float y, float z, float a, float b, float c){
	return a*x+b*y+c*z;
}

float mag(float x, float y, float z){
	
	
	return sqrt(x*x + y*y + z*z);
}

uint8_t RayPlaneIntersect(struct ray* r, struct plane* p){
	
	float dot_r_n, dot_po_ro_n;
	float prx, pry, prz;
	
	dot_r_n = dot(r->dx,r->dy,r->dz,p->nx,p->ny,p->nz);	//dot product between ray dir and normal
	
	if(fabs(dot_r_n) < INTERSECT_THRESHOLD){
		//line and plane are parallel
		r->dist = MAX_RAY_DIST;
		return 0;
		
	}
	
	//distance between ray origin and plane origin
	prx = p->ox - r->ox;	pry = p->oy - r->oy;	prz = p->oz - r->oz;
	
	r->dist = dot(prx,pry,prz,p->nx,p->ny,p->nz) / dot_r_n;	//get distance
	
	if(r->dist < 0.0)
		return 0;
	
	//calculate intersection point
	r->ix = r->ox + r->dist*r->dx;
	r->iy = r->oy + r->dist*r->dy;
	r->iz = r->oz + r->dist*r->dz;
	
	return 1;
}

uint8_t RaySphereIntersect(struct ray* r, struct sphere* s){
	
	float doc_x, doc_y, doc_z;
	float dot1, del, d_plus, d_minus;
	
	doc_x = r->ox - s->ox;	doc_y = r->oy - s->oy;	doc_z = r->oz - s->oz;
	
	dot1 = dot(doc_x, doc_y, doc_z, r->dx, r->dy, r->dz);
	
	del = dot1*dot1 - (dot(doc_x, doc_y, doc_z, doc_x, doc_y, doc_z) - s->radius*s->radius);
	
	if(del<0.0f)
		return 0;
	
	d_plus = -dot1 + sqrt(del);
	d_minus = -dot1 - sqrt(del);
	
	if(d_minus > 0.0f)
		r->dist = d_minus;
	else
		r->dist = d_plus;
	
	r->ix = r->ox + r->dist*r->dx;
	r->iy = r->oy + r->dist*r->dy;
	r->iz = r->oz + r->dist*r->dz;
	
	return 1;
}

void ColGradient(uint8_t R1, uint8_t G1, uint8_t B1, 
				 uint8_t R2, uint8_t G2, uint8_t B2, 
				 uint8_t* Rr, uint8_t *Gr, uint8_t *Br,
				 float factor){
	
	*Rr = (uint8_t)lround((1.0f-factor) * R1 + factor * R2);
	*Gr = (uint8_t)lround((1.0f-factor) * G1 + factor * G2);
	*Br = (uint8_t)lround((1.0f-factor) * B1 + factor * B2);
	
}

float clamp(float x, float min, float max){
	
	if(x < min)
		return min;
	if(x>max)
		return max;
	else
		return x;
	
}

int main(int argc, char* argv[]){
	
	int img_x, img_y;
	float aspect;
	int x, y, k;
	int n_pixels;

	struct ray *rays = NULL;

	uint8_t hit = 0;
	float fog = 0.0f;
	
	float old_dist;
	
	int checker_x, checker_z;
	int new_y = 0;
	
	uint8_t Rsky, Gsky, Bsky;

	struct sphere sph = {-2.0f,-0.25f,5.0f,0.75f};
	
	struct sun light = {0.5f, -0.5f, 0.5f};
	
	light.dx /= mag(light.dx,light.dy,light.dz);
	light.dy /= mag(light.dx,light.dy,light.dz);
	light.dz /= mag(light.dx,light.dy,light.dz);
	
	img_x = 640;
	img_y = 480;
	
	aspect = 1.0f*img_y/img_x;
	
	BMP* frame = BMP_Create(img_x,img_y,32);
	
	n_pixels = img_x*img_y;
	
	float camFOV = M_PI/2.0f;	//camera Field of view in radians
	
	struct plane floor = {0.0f,-1.0f,0.0f,
						  0.0f,1.0f,0.0f};
						  
	rays = calloc(n_pixels,sizeof(struct ray));
		
	for(y = 0; y < img_y; y++){
		
		new_y = img_y-y;
		
		ColGradient(255, 255, 255, 0, 32, 255, &Rsky, &Gsky, &Bsky, fabs(2*(1.0f*y)/img_y-1.0f));
			 
		for(x = 0; x < img_x; x++){
			
			old_dist = MAX_RAY_DIST;
			
			BMP_SetPixelRGB(frame, x, new_y, Rsky, Gsky, Bsky);
			
			k = x + y*img_x;
			
			rays[k].ox = 0.0f;
			rays[k].oy = 0.0f;
			rays[k].oz = 0.0f;
			
			rays[k].dx = sin(camFOV*(-0.5f+(1.0f*x)/img_x));
			rays[k].dy = sin(camFOV*aspect*(-0.5f+(1.0f*y)/img_y));
			rays[k].dz = 1.0f;
			
			RayNormalize(&rays[k]);
			
			hit = RaySphereIntersect(&rays[k], &sph);
			
			if(hit && old_dist > rays[k].dist){
				
				float nx, ny, nz, dot1;
				
				nx = sph.ox - rays[k].ix;
				ny = sph.oy - rays[k].iy;
				nz = sph.oz - rays[k].iz;
				
				dot1 = clamp(dot(nx,ny,nz,light.dx,light.dy,light.dz),0.3f,1.0f);

				old_dist = rays[k].dist;
				
				fog = 1.0-(rays[k].dist-mag(sph.ox,sph.oy,sph.oz))/(mag(sph.ox,sph.oy,sph.oz)+sph.radius);
				BMP_SetPixelRGB(frame, x, new_y, 200*fog*dot1, 200*fog*dot1, 200*fog*dot1);
				
				
				
			}
			
			hit = RayPlaneIntersect(&rays[k], &floor);
			
			//printf("%d, %f\n",k,rays[k].dist);
			fog = 1.0-rays[k].dist/100.0f;
			
			if(hit){
				
				if(old_dist > rays[k].dist){
				
					checker_x = ((int)abs(lround(rays[k].ix)) % 2);
					checker_z = ((int)abs(lround(rays[k].iz)) % 2);
					
					//printf("%d %d\n",checker_x, checker_z);
					
					struct ray shadow;
					
					shadow.ox = rays[k].ix;
					shadow.oy = rays[k].iy;
					shadow.oz = rays[k].iz;
					
					shadow.dx = -light.dx;
					shadow.dy = -light.dy;
					shadow.dz = -light.dz;
					
					hit = RaySphereIntersect(&shadow, &sph);
					fog = clamp(shadow.dist/(2*sph.radius),0.3f,1.0f);
					
					if(hit){
						if(checker_x^checker_z){
							BMP_SetPixelRGB(frame, x, new_y, 255*fog, 255*fog, 255*fog);
						}	
						else 
							BMP_SetPixelRGB(frame, x, new_y, 255*fog, 0*fog, 128*fog);
						
					}
					else{
						if(checker_x^checker_z){
							BMP_SetPixelRGB(frame, x, new_y, 255, 255, 255);
						}	
						else 
							BMP_SetPixelRGB(frame, x, new_y, 255, 0, 128);
					}
					
				}		
				
			}
		}
		
	}
	
	
	
	BMP_WriteFile(frame, "out.bmp");
	
	free(rays);
	
	BMP_Free(frame);
	
	return EXIT_SUCCESS;
	
}

